---
title: Uncle Bob
comments: false
publishdate: "2021-06-27"
lastmod: "2021-06-27"
---

Uncle Bob is the nickname of Robert C. Martin, writer of many famous books.

There are 6 videos in this series, where he explains about:
- Video 1: Clean Coding
- Video 2: Bad comments, Naming, Length of a line
- Video 3: 
- Video 4: Test Driven Development
- Video 5: Software Architecture
- Video 6: Agile

At the beginning of each video he talked about non relevant science stuff (which I think can just be skipped).

The way he talked about how software is soft and customers not knowing what they want just structs me, IT IS TRUE!

{{< youtube 7EmboKQH8lM >}}
