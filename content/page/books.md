---
title: Books on Programming
comments: false
publishdate: "2021-06-25"
lastmod: "2021-06-27"
---

### Books that I have read
1. [Head First Design Patterns]({{< relref "post/2021-06-25-head-first-design-pattern.md" >}}) by _Eric Freeman_

### Books that I am reading
1. Refactoring by Martin Fowler
2. Clean Architecture by Robert C. Martin
3. Clean Code by Robert C. Martin

### Books that I want to read
1. Domain-Driven Design by Eric Evans
2. Head First Agile by Andrew Stellman, Jennifer Greene
3. The Clean Coder by Robert C. Martin
4. The Pragmatic Programmer by David Thomas, Andrew Hunt
5. Domain-Driven Distilled by Vaughn Vernon
6. Beautiful Code edited by Andy Oram, Greg Wilson
7. The Art of Readable Code by Dustin Boswell, Trevor Foucher
