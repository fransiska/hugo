![Build Status](https://gitlab.com/fransiska/hugo/badges/master/pipeline.svg)

The blog is at [https://fransiska.gitlab.io/hugo/](https://fransiska.gitlab.io/hugo/)

## About

[ci]: https://about.gitlab.com/gitlab-ci/
[hugo]: https://gohugo.io
[install]: https://gohugo.io/overview/installing/
[documentation]: https://gohugo.io/overview/introduction/
[userpages]: http://doc.gitlab.com/ee/pages/README.html#user-or-group-pages
[projpages]: http://doc.gitlab.com/ee/pages/README.html#project-pages
[post]: https://about.gitlab.com/2016/04/07/gitlab-pages-setup/#custom-domains

The theme used is adapted from http://themes.gohugo.io/beautifulhugo/
